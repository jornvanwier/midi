import midi
from utils import read_txt


def main():
    tracks = read_txt('input/F.txt')
    tempo = 70

    # playing all tracks at the same time doesn't sound amazing
    tracks = [tracks[0]]

    # midi_path = input('MIDI:\n').strip()
    # tracks, tempo = midi.decode(midi_path)

    # truncate data
    tracks = [
        track[-6_000:]
        for track
        in tracks
    ]

    midi_file = midi.encode(tracks, tempo=tempo)
    midi.play(midi_file)


if __name__ == '__main__':
    main()
