import os
import platform
import tempfile
from typing import Optional

from midiutil import MIDIFile


def write(midi: MIDIFile, path: str) -> None:
    with open(path, 'wb') as output_file:
        midi.writeFile(output_file)


def synth(midi_path: str, ogg_path: Optional[str] = None) -> str:
    """
    Create a .ogg file from the provided MIDI path.

    :param midi_path: Path to the MIDI file.
    :param ogg_path: Path to the resulting ogg files. .ogg is placed in the same directory as MIDI if not provided.
    :return: The path to the created file.
    """
    if ogg_path is None:
        root, _ = os.path.splitext(midi_path)
        ogg_path = f'{root}.ogg'

    print(f'Synthesizing {ogg_path}')
    os.system('fluidsynth '
              '-nli '
              '-r 48000 '
              '-o synth.cpu-cores=4 '
              '-T oga '
              f'-F {ogg_path} '
              f'/usr/share/soundfonts/FluidR3_GM.sf2 {midi_path}')

    return ogg_path


def play(midi: MIDIFile):
    """
    Automatically play the MIDI file.
    :param midi: The MIDI file.
    """
    file_name = tempfile.mktemp(suffix='.midi')

    write(midi, file_name)

    play_fn = {
        'Linux': _play_linux,
        'Windows': _play_windows,
    }[platform.system()]
    play_fn(file_name)


def _play_linux(midi_path: str):
    ogg_path = synth(midi_path)
    os.system(f'xdg-open {ogg_path}')


def _play_windows(midi_path: str):
    # TODO open on windows
    pass
