from typing import Optional, Tuple, List

from mido import MidiFile, MidiTrack

from utils import flat_map


def decode(path: str) -> Tuple[List[List[int]], Optional[int]]:
    file = read(path)

    for msg in file.tracks[0]:
        print(f'meta {msg.is_meta} type {msg.type}')

    data = [decode_track(track) for track in file.tracks[1:]]
    return data, find_tempo(file.tracks[0])


def decode_track(track: MidiTrack) -> List[int]:
    notes = list(flat_map(lambda msg: [msg.note] * msg.time, filter(lambda msg: msg.type == 'note_on', track)))
    return notes


def read(path: str) -> MidiFile:
    return MidiFile(path)


def find_tempo(track: MidiTrack) -> Optional[float]:
    for msg in track:
        print(f'meta {msg.is_meta} type {msg.type}')
        if msg.is_meta and msg.type == 'set_tempo':
            return msg.tempo / 1_000

    return None
