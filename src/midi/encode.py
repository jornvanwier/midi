from typing import Collection

from midiutil import MIDIFile

from utils import run_length_encode


def encode(data: Collection[Collection[int]],
           channel=0,
           time=0,
           duration=1,
           tempo=500,
           volume=127,
           ) -> MIDIFile:
    """

    :param data: Input data to encode.
    :param channel: Channel in MIDI file
    :param time: In beats
    :param duration: In beats
    :param tempo: In BPM
    :param volume: 0-127, as per the MIDI standard
    :return: MIDI file
    """

    if tempo is None:
        raise TypeError('Tempo not provided')

    midi = MIDIFile(len(data))
    for i, track in enumerate(data):
        midi.addTempo(i, time, tempo)

        for j, (pitch, count) in enumerate(run_length_encode(track)):
            # pitch 0 means silence
            midi.addNote(i, channel, pitch, time + j, duration * count, volume if pitch > 0 else 0)

    return midi
