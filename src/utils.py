from typing import Iterable, TypeVar, Callable

T = TypeVar('T')


def run_length_encode(items: Iterable[T]) -> Iterable[T]:
    iterator = iter(items)
    current = next(iterator)
    count = 1

    try:
        while True:
            next_val = next(iterator)
            if next_val == current:
                count += 1
            else:
                yield current, count
                current = next_val
                count = 1
    except StopIteration:
        yield current, count
        return


def read_txt(path: str):
    with open(path) as f:
        lines = f.readlines()

    return list(zip(
        *(
            (
                int(note.strip())
                for note
                in line.split('\t')
            )
            for line
            in lines
        )
    ))


def flat_map(f: Callable[..., Iterable[T]], xs: Iterable) -> Iterable[T]:
    return (y for ys in xs for y in f(ys))
